#ifdef WINDOWS
#define _WIN32_WINNT 0x0603 //Change to whatever your Windows version is, just as long as it's > 0x0501!

//#include <errhandlingapi.h> //Sometimes needed, but commented out for now
#include <ws2tcpip.h> //Link to ws2_32
//#include <wspiapi.h> //Sometimes needed, but commented out for now
#include <winsock2.h> //ws2_32 May need to drop this
#include <winsock.h> //Link to winsock32 or ws2_32
#include <windows.h>
#include <winbase.h> //Link to kernel32
#else
#include <unistd.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <signal.h>
#include <sys/select.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#ifndef AF_NETBIOS
#define AF_NETBIOS 17
#endif
#ifndef AF_IRDA
#define AF_IRDA 26
#endif
#ifndef AF_BTH
#define AF_BTH 32
#endif

#define LBUFSIZE 8096

#ifdef INET6_ADDRLEN
#define MAXIP INET6_ADDRLEN
#else
#define MAXIP 256
#endif

int ExitType = 0;
int interrupted = 0;

#ifdef WINDOWS
// Handler function will be called on separate thread!
static BOOL WINAPI console_ctrl_handler(DWORD dwCtrlType)
{
  switch (dwCtrlType)
  {
  case CTRL_C_EVENT: // Ctrl+C
  case CTRL_BREAK_EVENT: // Ctrl+Break
    interrupted = 1;
    return TRUE;
    break;
  case CTRL_CLOSE_EVENT: // Closing the console window
    WSACleanup();
    break;
  case CTRL_LOGOFF_EVENT: // User logs off. Passed only to services!
    break;
  case CTRL_SHUTDOWN_EVENT: // System is shutting down. Passed only to services!
    break;
  }

  // Return TRUE if handled this message, further handler functions won't be called.
  // Return FALSE to pass this message to further handlers until default handler calls ExitProcess().
  return FALSE;
}
#else
void console_sigint_handler(int sig)
{
    interrupted = 1;
    signal(SIGINT,console_sigint_handler);
}
#endif

void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) return &(((struct sockaddr_in *) sa)->sin_addr);
    else if (sa->sa_family == AF_INET6) return &(((struct sockaddr_in6 *)sa)->sin6_addr); //May be unsupported in Windows, might be sockaddr_in6_old with ws2ipdef.h
    else 
    {
        fprintf(stderr,"Unimplemented Address Type %d for GetInAddress!\n",sa->sa_family);
        return NULL;
    }
}

#ifdef WINDOWS
int prerror(char *errortext,BOOL isnet)
#else
int prerror(char *errortext,int dummy)
#endif
{
    char errmsg[1024];
#ifdef WINDOWS
    DWORD err;
    if (isnet) err = WSAGetLastError();
    else err = GetLastError();
    if (err==109)interrupted = 1;
    DWORD ret = FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM,NULL,err,0,errmsg,1023,NULL); //This function seems to work, but I'm not 100% sure of it.
#else
    int ret;
    if (errno==EINTR)
    {
        strcpy(errmsg, "Interrupted by signal");
    }
    else sprintf(errmsg,"%s",strerror(errno));
    ret = strlen(errmsg);
    int err = errno;
#endif
    fprintf(stderr, "%s (%d): %s\n",errortext,err,errmsg);
    return (int) ret;
}

#ifdef WINDOWS
PCSTR XPinet_ntop(INT Family, VOID *pAddr, PSTR pStringBuf, size_t StringBufSize)
{
    //MinGW32-compatible inet_ntop
    /*
    INT WSAAPI WSAAddressToStringA(
      LPSOCKADDR          lpsaAddress,
      DWORD               dwAddressLength,
      LPWSAPROTOCOL_INFOA lpProtocolInfo,
      LPSTR               lpszAddressString,
      LPDWORD             lpdwAddressStringLength
    );
    */
    DWORD dwAddressLength, dwAddressStringLength = (DWORD) StringBufSize;
    void *sa;
    if (Family == AF_INET)
    { 
        struct sockaddr_in sai = {0};
        sai.sin_family = Family;
        memcpy(&(sai.sin_addr),(struct in_addr *) pAddr,sizeof(struct in_addr));
        dwAddressLength = sizeof(struct sockaddr_in);
        sa = (void *) &sai;
    }
    else if (Family == AF_INET6) 
    {
        struct  sockaddr_in6 sai6 = {0};  //May still need to fix all this.
        sai6.sin6_family = Family;
        memcpy(&(sai6.sin6_addr),(struct in6_addr *) pAddr,sizeof(struct in6_addr));
        dwAddressLength = sizeof(struct sockaddr_in6);
        sa = (void *) &sai6;
    }
    int ret = WSAAddressToStringA((LPSOCKADDR)sa,dwAddressLength,NULL,pStringBuf,&dwAddressStringLength); 
    if (ret == 0) return pStringBuf;
    fprintf(stderr,"Inet_ntop error %d\n",ret);
    return NULL;
}

PCSTR XPWRAPinet_ntop(INT Family, VOID *pAddr, PSTR pStringBuf, size_t StringBufSize)
{
    const char *addr = XPinet_ntop( Family,  pAddr,  pStringBuf,  StringBufSize);
#else
const char *WRAPinet_ntop(int family,const void *paddr, char *pstringbuf, socklen_t stringbufsize)
{
    const char *addr = inet_ntop(family,paddr,pstringbuf,stringbufsize);
#endif
    if (addr) return addr;
    prerror("Error getting Remote Address",1); //PError seems to not work with this on Windows...
    return NULL;
}

#ifdef WINDOWS
BOOL WINAPI WriteOutput(
  /*_In_*/             HANDLE  hOutput,
  /*_In_*/       const VOID    *lpBuffer,
  /*_In_*/             DWORD   nNumberOfCharsToWrite,
  /*_Out_opt_*/        LPDWORD lpNumberOfCharsWritten,
  /*_Reserved_*/       LPVOID  lpReserved
)
{
    if (GetFileType(hOutput) == FILE_TYPE_CHAR)
    {
        return WriteConsole(hOutput,lpBuffer,nNumberOfCharsToWrite,lpNumberOfCharsWritten,lpReserved);
    }
    else if (GetFileType(hOutput) == FILE_TYPE_DISK)
    {
        return WriteFile(hOutput,lpBuffer,nNumberOfCharsToWrite,lpNumberOfCharsWritten,NULL);
    }
    else if (GetFileType(hOutput) == FILE_TYPE_PIPE)
    {
        return WriteFile(hOutput,lpBuffer,nNumberOfCharsToWrite,lpNumberOfCharsWritten,NULL);
    }
    else if (GetFileType(hOutput) == FILE_TYPE_REMOTE) //Might not work?
    {
        return WriteFile(hOutput,lpBuffer,nNumberOfCharsToWrite,lpNumberOfCharsWritten,NULL);
    }
    else
    {
        SetLastError(120);//Call Not Implemented
        return FALSE;
    }
}
#endif

int main(int argc, char *argv[])
{
#ifdef WINDOWS
    WSADATA wsaData; //Might be WSAData on some Windows versions?
#endif
    int ret;

    if (argc<2||(argc == 2 && strcmp(argv[1],"/version")!=0)||strcmp(argv[1],"/?")==0)
    {
        printf("Usage:\n  %s <IPAddress> <Port> [<iptype>]\nWhere iptype forces either IPv4 (set to 4) or IPv6 (set to 6).\nE.G. '%s 127.0.0.1 8023' or '%s 192.168.0.1 8022 4'.\n", argv[0], argv[0], argv[0]);
        return 0;
    }
    else if (argc > 1 && strcmp(argv[1],"/version")==0)
    {
        printf("0.01.00\n");
        return 0;
    }
    
#ifdef WINDOWS
    if ((ret = WSAStartup(MAKEWORD(2,0), &wsaData)) != 0) //Might need to make this (1,1)?
    {
        fprintf(stderr, "WSAStartup Failed (Error Code: 0x%X)!\n",ret);
        return 2;
    }

    SetConsoleCtrlHandler(console_ctrl_handler, TRUE);
#else
    signal(SIGINT,console_sigint_handler);
#endif

    int address_family = AF_UNSPEC;
    if (argc>3)
    {
        switch (argv[3][0])
        {
            case '4':
              address_family = AF_INET;
            break;
            case '6':
              address_family = AF_INET6;
            break;
            case 'B':
              address_family = AF_BTH;
            break;
            case 'I':
              address_family = AF_IRDA;
            break;
            case 'N':
              address_family = AF_NETBIOS;
            break;
            case '0':
              address_family = AF_UNSPEC;
            break;
            default:
              fprintf(stderr, "Invalid Address Family %s!\n",argv[3]);
              ExitType = 255;
              goto CleanUp;
            break;
        }
    }

    char *portS = argv[2];
    char *hostS = argv[1];
    int porti = atoi(portS);
    int sockfd;
#ifdef WINDOWS
    unsigned long
#endif
    int numbytes,bwritten,starti,totalwritten;
    char buf[LBUFSIZE];
    struct addrinfo hints, *servinfo, *p;
    char sip[MAXIP];
#ifdef SOLARIS
    char yes='1';
#else
    int yes=1;
#endif

    memset(&hints,0,sizeof(hints));
    hints.ai_family = address_family;
    hints.ai_socktype = SOCK_STREAM;

    if ((ret = getaddrinfo(hostS, portS, &hints, &servinfo)) != 0)
    {
#ifdef WINDOWS
        fprintf(stderr, "Error getting address info: %s!\n",gai_strerrorA(ret));
#else
        fprintf(stderr, "Error getting address info: %s!\n",gai_strerror(ret));
#endif
        ExitType = 3;
        goto CleanUp;
    }

    for (p=servinfo; p!=NULL; p=p->ai_next)
    {
        sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (sockfd < 0)
        {
            perror("Socket of client");
            continue;
        }
        setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (char *) &yes, sizeof(int)); //Might need tweaking for some Windows variants.
        if (connect(sockfd, p->ai_addr, p->ai_addrlen) < 0)
        {
#ifdef WINDOWS
            closesocket(sockfd);
#else
            close(sockfd);
#endif
            perror("Connection process of client");
            continue;
        }
        break;
    }

    if (p == NULL)
    {
        fprintf(stderr, "Failed to Connect!\n");
        ExitType = 4;
        goto CleanUp;
    }
    
#ifdef WINDOWS
    XPWRAPinet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),sip,sizeof(sip));
#else
    WRAPinet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),sip,sizeof(sip));
#endif
#ifdef WINDOWS
    TIMEVAL tv;
    DWORD err;
#else
    struct timeval tv;
#endif
    fd_set master;
    fd_set read_fds;
    FD_ZERO(&master);
    FD_ZERO(&read_fds);
    FD_SET(sockfd, &master);

    fprintf(stderr,"Connecting to %s on port %d.\n",sip,porti);

    freeaddrinfo(servinfo);

    while (!interrupted)
    {
DoSelect:
        read_fds = master;
        tv.tv_sec = 2;
        tv.tv_usec = 0;
#ifdef WINDOWS
        if ((ret=select(sockfd+1,&read_fds,NULL,NULL,&tv))==SOCKET_ERROR)
#else
        if ((ret=select(sockfd+1,&read_fds,NULL,NULL,&tv))<0)
#endif
        {
            perror("Selecting");
#ifndef WINDOWS
            if (errno == EINTR)
            {
                if (!interrupted) goto DoSelect;
                else break;
            }
#endif
            ExitType = 5;
            break;
        }
        if (interrupted) break;
        if (!ret) goto DoSelect;
        if ((numbytes = recv(sockfd,buf,LBUFSIZE-1,0)) < 0)
        {
            perror("Receiving Data");
            ExitType=6;
            break;
        }
        else if (!numbytes)
        {
            fprintf(stderr,"Socket Closed.\n");
            ExitType=0;
            break;
        }
        else
        {
            if (numbytes < LBUFSIZE)
            {
                buf[numbytes] = 0;
                starti=0;
                totalwritten=0;
                do
                {
#ifdef WINDOWS
                    WriteOutput(GetStdHandle(STD_OUTPUT_HANDLE),buf+starti,numbytes-totalwritten,&bwritten,NULL);
#else
                    bwritten = write(STDOUT_FILENO,buf+starti,numbytes-totalwritten);
                    if (bwritten<0) break;
#endif
                    totalwritten+=bwritten;
                    if (totalwritten<numbytes)
                    {
                        starti += bwritten;
                    }
                } while (totalwritten<numbytes && !interrupted);
#ifdef DEBUG
                fprintf(stderr,"%s",buf);
#endif
            }
        }
    }
#ifdef WINDOWS
    closesocket(sockfd);
#else
    close(sockfd);
#endif
CleanUp:
#ifdef WINDOWS
    if ((ret = WSACleanup())!=0)
    {
        fprintf(stderr, "WSACleanup Failed (Error Code: 0x%X)!\n",ret);
        if (ret == WSAEINPROGRESS)
        {
            WSACancelBlockingCall(); //Causes a Warning but still works.
            goto CleanUp;
        }
    }
#else
    signal(SIGINT, SIG_DFL);
#endif
    return ExitType;
}
