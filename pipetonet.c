#ifdef WINDOWS
#define _WIN32_WINNT 0x0603 //Change to whatever your Windows version is, just as long as it's > 0x0501!

#include <ws2tcpip.h> //Link with ws2_32
//#include <wspiapi.h> //Sometimes needed, but commented out for now
#include <winsock2.h> //Link with ws2_32.  May need to drop this
#include <winsock.h> //Link with winsock32 or ws2_32
#include <windows.h>
#include <winbase.h> //Link with kernel32
#else
#include <errno.h>
#include <signal.h>
#ifdef COHERENT
#include <termio.h>
#include <sys/ioctl.h>
#else
#include <termios.h>
#endif
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/types.h>
#include <netdb.h>
#ifndef SOCKET_ERROR
#define SOCKET_ERROR -1
#endif
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#ifndef AF_NETBIOS
#define AF_NETBIOS 17
#endif
#ifndef AF_IRDA
#define AF_IRDA 26
#endif
#ifndef AF_BTH
#define AF_BTH 32
#endif


#define LBUFSIZE 8096


int ExitType = 0;
int interrupted = 0;


int alwaysnonnegative(int n)
{
    if (n<0)
    {
#ifdef DEBUG
        fprintf(stderr,"<0\n");
#endif
        return 0;
    }
    return n;
}

int minnum(int a, int b)
{
    if (a<b) return a;
    return b;
}

#ifdef WINDOWS
// Handler function will be called on separate thread!
static BOOL WINAPI console_ctrl_handler(DWORD dwCtrlType)
{
  switch (dwCtrlType)
  {
  case CTRL_C_EVENT: // Ctrl+C
  case CTRL_BREAK_EVENT: // Ctrl+Break
    interrupted = 1;
    return TRUE;
    break;
  case CTRL_CLOSE_EVENT: // Closing the console window
    WSACleanup();
    break;
  case CTRL_LOGOFF_EVENT: // User logs off. Passed only to services!
    break;
  case CTRL_SHUTDOWN_EVENT: // System is shutting down. Passed only to services!
    break;
  }

  // Return TRUE if handled this message, further handler functions won't be called.
  // Return FALSE to pass this message to further handlers until default handler calls ExitProcess().
  return FALSE;
}
#else
void console_sigint_handler(int sig)
{
    interrupted = 1;
    signal(SIGINT,console_sigint_handler);
}
void console_sigterm_handler(int sig)
{
#ifdef COHERENT
    ioclt(STDIN_FILENO, TCSETA, &oldt); /*tcsetattr TCSANOW equivalent*/
#else
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
#endif
    signal(SIGTERM,SIG_DFL);
    signal(SIGINT, SIG_DFL);
    exit(254);
}
#endif

void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) return &(((struct sockaddr_in *) sa)->sin_addr);
    return &(((struct sockaddr_in6 *)sa)->sin6_addr); //May be unsupported, might be sockaddr_in6_old with ws2ipdef.h
}

#ifdef WINDOWS
int prerror(char *errortext,BOOL isnet)
#else
int prerror(char *errortext,int dummy)
#endif
{
    char errmsg[1024];
#ifdef WINDOWS
    DWORD err;
    if (isnet) err = WSAGetLastError();
    else err = GetLastError();
    if (err==109)interrupted = 1;
    DWORD ret = FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM,NULL,err,0,errmsg,1023,NULL); //This function seems to work, but I'm not 100% sure of it.
#else
    int ret;
    if (errno==EINTR)
    {
        strcpy(errmsg, "Interrupted by signal");
    }
    else sprintf(errmsg,"%s",strerror(errno));
    int err=errno;
    ret = strlen(errmsg);
#endif
    fprintf(stderr, "%s (%d): %s\n",errortext,err,errmsg);
    return (int) ret;
}

#ifdef WINDOWS
PCSTR XPinet_ntop(INT Family, VOID *pAddr, PSTR pStringBuf, size_t StringBufSize)
{
    //MinGW32-compatible inet_ntop
    /*
    INT WSAAPI WSAAddressToStringA(
      LPSOCKADDR          lpsaAddress,
      DWORD               dwAddressLength,
      LPWSAPROTOCOL_INFOA lpProtocolInfo,
      LPSTR               lpszAddressString,
      LPDWORD             lpdwAddressStringLength
    );
    */
    DWORD dwAddressLength, dwAddressStringLength = (DWORD) StringBufSize;
    void *sa;
    if (Family == AF_INET)
    { 
        struct sockaddr_in sai = {0};
        sai.sin_family = Family;
        memcpy(&(sai.sin_addr),(struct in_addr *) pAddr,sizeof(struct in_addr));
        dwAddressLength = sizeof(struct sockaddr_in);
        sa = (void *) &sai;
    }
    else if (Family == AF_INET6) 
    {
        struct  sockaddr_in6 sai6 = {0};  //This might not work reliably?
        sai6.sin6_family = Family;
        memcpy(&(sai6.sin6_addr),(struct in6_addr *) pAddr,sizeof(struct in6_addr));
        dwAddressLength = sizeof(struct sockaddr_in6);
        sa = (void *) &sai6;
    }
    
    int ret = WSAAddressToStringA((LPSOCKADDR)sa,dwAddressLength,NULL,pStringBuf,&dwAddressStringLength); //Is the NULL the issue? WSAEINVAL  No.
    if (ret == 0) return pStringBuf;
    fprintf(stderr,"Inet_ntop error %d\n",ret);
    return NULL;
}

PCSTR XPWRAPinet_ntop(INT Family, VOID *pAddr, PSTR pStringBuf, size_t StringBufSize)
{
    const char *addr = XPinet_ntop( Family,  pAddr,  pStringBuf,  StringBufSize);
#else
const char *WRAPinet_ntop(int family,const void *paddr, char *pstringbuf, socklen_t stringbufsize)
{
    const char *addr = inet_ntop(family,paddr,pstringbuf,stringbufsize);
#endif
    if (addr) return addr;
    prerror("Error getting Remote Address",1); //PError seems to not work with this on Windows...
    return NULL;
}

#ifdef WINDOWS
int datainpipe()
{
    DWORD NumBAvail, bi, anerr;
    int ret;
    char buf[LBUFSIZE];
    HANDLE inp = GetStdHandle(STD_INPUT_HANDLE);
    if (GetFileType(inp)!=FILE_TYPE_PIPE)
    {
#ifdef DEBUG
        fprintf(stderr,"NoPipe\n");
#endif        
        return -1;
    }
    if (!PeekNamedPipe(inp,buf,LBUFSIZE-1,&bi,&NumBAvail,NULL))
    {
        anerr = GetLastError();
        if (anerr == 109)//Broken Pipe
        {
            interrupted = 1;
        }
        ret = -2-anerr;
    }
    else ret = ((int) NumBAvail);
    if (bi>0 && bi<LBUFSIZE) buf[bi]=0;
#ifdef DEBUG
    if (bi!=0) fprintf(stderr,"bi=%lu\n",bi);
#endif        
    return ret;
}
#endif


int main(int argc, char *argv[])
{
#ifdef WINDOWS
    WSADATA wsaData; //Might be WSAData depending on Windows version?
#endif
    int ret,sret,hit=0;

#ifdef WINDOWS
    if (argc<2||(argc == 2 && strcmp(argv[1],"/version")!=0)||strcmp(argv[1],"/?")==0)
#else
    if (argc<2||strcmp(argv[1],"--help")==0)
#endif
    {
        printf("Usage:\n  %s <Port> [<iptype>]\nWhere iptype forces either IPv4 (set to 4) or IPv6 (set to 6).\nE.G. '%s 8022' or '%s 8023 4'.\n", argv[0], argv[0], argv[0]);
        return 0;
    }
#ifdef WINDOWS
    else if (argc > 1 && strcmp(argv[1],"/version")==0)
#else
    else if (argc > 1 && strcmp(argv[1],"--version")==0)
#endif
    {
        printf("0.01.00\n");
        return 0;
    }

#ifdef WINDOWS
    if (GetFileType(GetStdHandle(STD_INPUT_HANDLE))!=FILE_TYPE_PIPE)
    {
        fprintf(stderr,"Error:  Input is not a pipe!\n");
        return 1;
    }
    
    if ((ret = WSAStartup(MAKEWORD(2,0), &wsaData)) != 0) //Might need to make this (1,1)?
    {
        fprintf(stderr, "WSAStartup Failed (Error Code: 0x%X)!\n",ret);
        return 2;
    }

    SetConsoleCtrlHandler(console_ctrl_handler, TRUE);
#else
    signal(SIGTERM,console_sigterm_handler);
    signal(SIGINT,console_sigint_handler);
#endif

    int address_family = AF_UNSPEC;
    if (argc>2)
    {
        switch (argv[2][0])
        {
            case '4':
              address_family = AF_INET;
            break;
            case '6':
              address_family = AF_INET6;
            break;
            case 'B':
              address_family = AF_BTH;
            break;
            case 'I':
              address_family = AF_IRDA;
            break;
            case 'N':
              address_family = AF_NETBIOS;
            break;
            case '0':
              address_family = AF_UNSPEC;
            break;
            default:
              fprintf(stderr, "Invalid Address Family %s!\n",argv[2]);
              ExitType = 255;
              goto CleanUp;
            break;
        }
    }

    char *portS = argv[1];
    int porti = atoi(portS);
    fd_set master;
    fd_set read_fds;
    int fdmax;
    int listener;
    int newfd;
    struct sockaddr_storage remoteaddr; //Client address
    socklen_t addrlen;
    char buf[LBUFSIZE];
#ifdef WINDOWS
    long unsigned
#endif
    int nbytes;

    char remoteIP[INET6_ADDRSTRLEN];
#ifdef SOLARIS
    char yes='1';
#else
    int yes=1;
#endif
    int i,j;
    struct addrinfo hints, *ai, *p;

    FD_ZERO(&master);
    FD_ZERO(&read_fds);

    memset(&hints,0,sizeof(hints));
    hints.ai_family = address_family;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    if ((ret = getaddrinfo(NULL, portS, &hints, &ai)) != 0)
    {
#ifdef WINDOWS
        fprintf(stderr, "Error getting address info: %s!\n",gai_strerrorA(ret));
#else
        fprintf(stderr, "Error getting address info: %s!\n",gai_strerror(ret));
#endif
        ExitType = 3;
        goto CleanUp;
    }

    for (p=ai; p!=NULL; p=p->ai_next)
    {
        listener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (listener < 0) continue;
        setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, (char *) &yes, sizeof(int)); //Might need tweaking for Windows.
        if (bind(listener, p->ai_addr, p->ai_addrlen) < 0)
        {
#ifdef WINDOWS
            closesocket(listener);
#else
            close(listener);
#endif
            continue;
        }
        break;
    }
    if (p == NULL)
    {
        fprintf(stderr, "Failed to bind!\n");
        ExitType = 4;
        goto CleanUp;
    }
    freeaddrinfo(ai);

    if (listen(listener,10)==-1)//Backlog of 10
    {
        perror("Listening"); //Might not work on Windows!  If so, change to prerror(,1)
        ExitType = 5;
        goto CleanUp;
    }

    FD_SET(listener, &master);
#ifndef WINDOWS
    FD_SET(STDIN_FILENO, &master);
#endif
    
    fdmax = listener;
    
#ifdef WINDOWS
    DWORD err;
    TIMEVAL tv;
#else
    unsigned long int err;
    struct timeval tv;
#endif

    while (!interrupted)
    {
DoSelect:
        read_fds = master;
#ifdef WINDOWS
        tv.tv_sec = 0;
        tv.tv_usec = 100000;
#else
        tv.tv_sec = 2;
        tv.tv_usec = 0;
#endif
        if ((ret = select(fdmax+1,&read_fds,NULL,NULL,&tv))==SOCKET_ERROR)
        {
            perror("Selecting"); //Might not work on Windows!  If so, change to prerror(,1)
#ifdef WINDOWS
            err = WSAGetLastError();
#else
            err = errno;
#endif
            fprintf(stderr,"Selecting: %lu\n",err);
#ifndef WINDOWS
            if (err == EINTR)
            {
                if (!interrupted) goto DoSelect;
                else break;
            }
#endif
            ExitType = 6;
            break;
        }
#ifdef WINDOWS
        //Read Pipe Here
        if ((sret = datainpipe()) > 0)
        {
#ifdef DEBUG
            fprintf(stderr,"Data available: %d Bytes\n",sret);
#endif
            if (!ReadFile(GetStdHandle(STD_INPUT_HANDLE),buf,alwaysnonnegative(minnum(sret,LBUFSIZE)),&nbytes,NULL))
            {
                prerror("Failed to read input",0);
            }
            else for (i=1;i<=fdmax;i++)
            {
                if (FD_ISSET(i,&master))
                {
                    //Send to i
                    (void)send(i,buf,nbytes,0);
                }
            }
        }
        else if (interrupted)
        {
            ExitType=0;
            break;
        }
        else if (sret == -1)
        {
            //Input not a pipe
            fprintf(stderr,"Error:  Input is not a pipe!\n");
            interrupted=1;
            ExitType=1;
            break;
        }
        else if (sret<-1)
        {
            //error
            fprintf(stderr,"Input Error %d\n",-1-ret);
        }
#else
#endif
        if (ret == 0 && !interrupted) goto DoSelect; //Allows for Ctrl+C Interrupt
        if (interrupted) break;
        for (i=0;i<=fdmax;i++)
        {
            if (FD_ISSET(i,&read_fds))
            {
                if (i==listener)
                {
                    //New Connection
                    addrlen = sizeof(remoteaddr);
                    newfd = accept(listener,(struct sockaddr *)&remoteaddr,&addrlen);
                    if (newfd <0) perror("Accepting"); //Might not work on all Windows versions!  If so, change to prerror(,1)
                    else
                    {
                        FD_SET(newfd,&master);
                        if (newfd>fdmax) fdmax = newfd;
#ifdef WINDOWS
                        fprintf(stderr,"New connection from %s on socket %d!\n",XPWRAPinet_ntop(remoteaddr.ss_family,get_in_addr((struct sockaddr *) &remoteaddr), remoteIP, INET6_ADDRSTRLEN), newfd);
#else
                        fprintf(stderr,"New connection from %s on socket %d!\n",WRAPinet_ntop(remoteaddr.ss_family,get_in_addr((struct sockaddr *) &remoteaddr), remoteIP, INET6_ADDRSTRLEN), newfd);
#endif
                    }
                }
#ifndef WINDOWS
                else if (i==STDIN_FILENO)
                {
                    if ((nbytes = read(STDIN_FILENO,buf,sizeof(char)*(LBUFSIZE-1)))<1)
                    {
                        if (nbytes<0) prerror("Failed to read input",0);
                        else
                        {
                            fprintf(stderr,"Warning: Input blank!  Assuming port closed.\n");
                            interrupted = 1;
                            break;
                        }
                    }
                    else for (i=0;i<=fdmax;i++)
                    {
                        if (FD_ISSET(i,&master) && i!=STDIN_FILENO && i!=listener)
                        {
                            //Send to i
                            (void)send(i,buf,nbytes,0);
                        }
                    }
#ifdef DEBUG
                        buf[nbytes]=0;fprintf(stderr,"nb=%d,b=%s.\n",nbytes,buf);
#endif
                    
                    
                    if (interrupted)
                    {
                        ExitType=0;
                        break;
                    }
                }
#endif
                else
                {
                    //Handle client request!
                    if ((nbytes = recv(i,buf,sizeof(buf),0))<=0)
                    {
                        //Error or connection closed...
                        if (nbytes == 0)
                        {
                            //Connection closed
                            fprintf(stderr,"Socket %d hung up.\n",i);
                        }
                        else
                        {
                            perror("Receiving"); //Might not work on Windows!  If so, change to prerror(,1)
                        }
#ifdef WINDOWS
                        closesocket(i);
#else
                        close(i);
#endif
                        FD_CLR(i, &master);
                    }
                    else
                    {
                        //Data in buf!
                        buf[nbytes] = 0;
                        fprintf(stderr,"Message from client %d: %s.\n",i,buf);
                    }
                }
            }
        }
    }
    for (i=0;i<=fdmax;i++) if (FD_ISSET(i,&master) && i != STDIN_FILENO)
    {
#ifdef WINDOWS
            closesocket(i);
#else
            close(i);
#endif
    }

CleanUp:
#ifdef WINDOWS
    if ((ret = WSACleanup())!=0)
    {
        fprintf(stderr, "WSACleanup Failed (Error Code: 0x%X)!\n",ret);
        if (ret == WSAEINPROGRESS)
        {
            WSACancelBlockingCall(); //Causes a Warning, but still works.
            goto CleanUp;
        }
    }
#else
    signal(SIGINT, SIG_DFL);
    signal(SIGTERM, SIG_DFL);
#endif
    return ExitType;
}






