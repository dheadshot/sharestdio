CFLAGS_WIN = -DWINDOWS
SOLARISLIBS = -lsnl -lsocket -lresolv

CFLAGS = ${CFLAGS_WIN}

BOTH_WINDOWS: nettoout.exe pipetonet.exe

BOTH_NIX: nettoout pipetonet



nettoout: nettoout.o
	gcc nettoout.o -o nettoout
	chmod +x nettoout

pipetonet: pipetonet.o
	gcc pipetonet.o -o pipetonet
	chmod +x pipetonet

nettoout.exe: nettoout.o
	gcc nettoout.o -o nettoout.exe -lws2_32 -lkernel32

pipetonet.exe: pipetonet.o
	gcc pipetonet.o -o pipetonet.exe -lws2_32 -lkernel32

pipetonet.o: pipetonet.c
	gcc ${CFLAGS} -c pipetonet.c

nettoout.o: nettoout.c
	gcc ${CFLAGS} -c nettoout.c
