# ShareStdIO

## Description

This project exists to easily share console I/O over a local area network.  It consists of two programs: `pipetonet`, which is a simple TCP server server that takes a piped input on stdin and sends it to any program that connects to it, and `nettoout`, which is a simple TCP client that connects to a host server and outputs everything it receives to stdout.  These programs are designed to be used together, but it should be possible to use them with other software instead.

## Usage

### PipeToNet

`pipetonet <Port> [<iptype>]`

Where iptype forces either IPv4 (set to 4) or IPv6 (set to 6).  E.G. `pipetonet 8022` or `pipetonet 8023 4`.

### NetToOut

`nettoout <IPAddress> <Port> [<iptype>]`

Where iptype forces either IPv4 (set to 4) or IPv6 (set to 6).  E.G. `nettoout 127.0.0.1 8023` or `nettoout 192.168.0.1 8022 4`.


## Building

ShareStdIO can be built for both Windows (with MinGW) and Linux (with GCC).

### Windows

Simply run `make BOTH_WINDOWS` to compile both programs for Windows.

### Linux

To compile for Linux, edit the `Makefile` and comment out line 4 so that `CFLAGS = ${CFLAGS_WIN}` becomes `#CFLAGS = ${CFLAGS_WIN}`, then run `make BOTH_NIX`.


## Licence

MIT License

Copyright (c) 2023 DHeadshot's Software Creations

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.